-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2023 at 10:24 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ikhwan`
--

-- --------------------------------------------------------

--
-- Table structure for table `eskul`
--

CREATE TABLE `eskul` (
  `nomor` int(11) NOT NULL,
  `nim` int(255) NOT NULL,
  `namaKegiatan` varchar(255) NOT NULL,
  `aktifKegiatan` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `eskul`
--

INSERT INTO `eskul` (`nomor`, `nim`, `namaKegiatan`, `aktifKegiatan`) VALUES
(1, 1002, 'Basket', 1),
(2, 1002, 'Basket', 1),
(3, 113, 'Bulu tangkis', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `no` int(11) NOT NULL,
  `nim` int(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `prodi` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `taat` varchar(50) NOT NULL,
  `kegiatan` varchar(255) NOT NULL,
  `ipk` int(55) NOT NULL,
  `prediksi` varchar(55) NOT NULL,
  `yang_nambahin` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`no`, `nim`, `nama`, `prodi`, `kelas`, `taat`, `kegiatan`, `ipk`, `prediksi`, `yang_nambahin`) VALUES
(1, 1002, 'Asep dadang', '', '1', '', '', 4, '', 1),
(2, 2, 'asep suherman', 'Rekayasa Perangkat Lunak', '22', '', '', 22, '', 1),
(4, 113, 'asep prikitiw', '', '113', '', '', 113, '', 3),
(5, 1110, 'ASEP SUHERMAN', 'Rekayasa Perangkat Lunak', '1', '', '', 4, '', 4),
(7, 23, '144', 'Rekayasa Perangkat Lunak', '11', '', '', 11, '', 1),
(8, 10001, 'Suparman', 'Kamplingan', '5', '', '', 4, '', 6),
(10, 1114, '112', '441', '12', 'a', 'a', 5, '', 1),
(11, 1441, 'Asep Kuncoro', 'Rekayasa Perangkat Lunak', '5', 'Tidak taat', 'Aktif', 4, '', 4),
(13, 10091, 'step by step', 'Akuntansi', '5', 'Tidak taat', 'Aktif', 4, 'Berprestasi', 4),
(14, 10095, 'Ferdy Sambo', 'Akuntansi', '1', 'Taat', 'Aktif', 4, 'Berprestasi', 15);

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE `matakuliah` (
  `nomor` int(11) NOT NULL,
  `kodeMK` varchar(255) NOT NULL,
  `namaMatkul` varchar(255) NOT NULL,
  `nilaiKredit` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`nomor`, `kodeMK`, `namaMatkul`, `nilaiKredit`) VALUES
(1, 'MK01', 'Tata Boga', 0),
(2, 'MK02', 'Tata Usaha', 0),
(3, 'MK03', 'Matematika Padu', 0),
(4, 'MK004', 'Seni Rupa\r\n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `namaeskul`
--

CREATE TABLE `namaeskul` (
  `nomor` int(11) NOT NULL,
  `kodeEskul` varchar(50) NOT NULL,
  `namaEskul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `namaeskul`
--

INSERT INTO `namaeskul` (`nomor`, `kodeEskul`, `namaEskul`) VALUES
(1, '', 'Basket'),
(2, '', 'Volly'),
(3, '', 'Bulu Tangkis'),
(4, '', 'Hockey');

-- --------------------------------------------------------

--
-- Table structure for table `nilaimahasiswa`
--

CREATE TABLE `nilaimahasiswa` (
  `nomor` int(11) NOT NULL,
  `nim` int(15) NOT NULL,
  `nilai1` int(15) NOT NULL,
  `nilai2` int(15) NOT NULL,
  `nilai3` int(15) NOT NULL,
  `nilai4` int(15) NOT NULL,
  `nilaiTotal` int(255) NOT NULL,
  `pencapaian` varchar(255) NOT NULL,
  `mataKuliah` text NOT NULL,
  `ipkTotal` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `nilaimahasiswa`
--

INSERT INTO `nilaimahasiswa` (`nomor`, `nim`, `nilai1`, `nilai2`, `nilai3`, `nilai4`, `nilaiTotal`, `pencapaian`, `mataKuliah`, `ipkTotal`) VALUES
(1, 2, 2, 13, 11, 5, 28, 'INI PENCAPAIAN', 'Tata Boga', '0'),
(2, 2, 3, 19, 18, 17, 57, 'D', 'Tata Usaha', '0'),
(5, 10001, 3, 2, 3, 19, 27, 'F', 'Tata Usaha', 'INI IPK TOTAL'),
(11, 10091, 22, 20, 20, 22, 84, 'AB', 'Tata Boga', '3.50'),
(12, 10095, 22, 22, 23, 24, 91, 'A', 'Tata Usaha', '4.00');

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `nomor` int(11) NOT NULL,
  `kodeProdi` varchar(55) NOT NULL,
  `namaProdi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`nomor`, `kodeProdi`, `namaProdi`) VALUES
(1, 'PR001', 'Rekayasa Perangkat Lunak'),
(2, 'PR002', 'Akuntansi'),
(3, 'PR003', 'Multimedia'),
(4, 'PR004', 'Kamplingan');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL,
  `nama_dosen` varchar(255) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `tanggal_daftar` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_users`, `nama_dosen`, `username`, `password`, `tanggal_daftar`) VALUES
(1, 'subeni', 'asep', 'asep123', '2023-01-16'),
(2, '123', '1234', '1234', '2023-01-17'),
(3, 'haruman', 'haruman123', 'haruman123', '2023-01-18'),
(4, 'kusogaki', 'kuso', 'kuso', '2023-01-23'),
(5, 'kuyang hunter', 'kuyang', '[C@6a63a5f2', '2023-01-23'),
(6, 'kuyang', 'kuyangkin', 'kuyangkin', '2023-01-23'),
(7, 'Aku bunda irna', 'aku', 'aku', '2023-01-24'),
(8, '1', '1', '$argon2id$v=19$m=65536,t=3,p=1$VM8IuSApyjPltuuxwYZJcA$Q67jiR9h0SGMrZgKUgvbf0fBYq5iZOC94PNV+ruM3KY', '2023-01-27'),
(9, '2', '2', '$argon2id$v=19$m=65536,t=3,p=1$SBH2TXCF5Hxu5bYEnt4AJQ$GSx3ORpFy06I7Ue26/3YHAV7ivZ0i0OOGsuqtpb/aeA', '2023-01-27'),
(10, '123', '123', '$argon2id$v=19$m=65536,t=3,p=1$WDbnk7pxTenSWYtv2iHLhQ$+lEbm6q2B9j6P5CpoFzydvWGUsFXxDeMD6JND5eTYEA', '2023-01-27'),
(11, '2', '2', '1$1piiJzFNt2NX4vpE4NCXfQ$8y00OTFN0DbkDdm8a1AA1Iap+qb6vPFyOa7pbP/0KVo', '2023-01-27'),
(12, '123', '4', '$argon2id$v=19$m=65536,t=3,p=1$WCChvfhnd7lFYv8PxJwozQ$BjHBrdYN0TtK9r657AGXKxfaSgGubhwQIL0NJqJ2bUg', '2023-01-27'),
(13, 'ai', 'ai', '[B@4881c5fe69a70287432f36d5723c3a7afb136fb371aca0073969fe334617f372bd9fed26', '2023-01-27'),
(14, 'qq', 'qq', '[B@34e09368e2161373148e06d792dda40b9e7bbeb2b61c3398d6b5771054d80dc4a79f4667', '2023-01-27'),
(15, 'sambo', 'sambo', 'sambo', '2023-01-28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `eskul`
--
ALTER TABLE `eskul`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `namaeskul`
--
ALTER TABLE `namaeskul`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `nilaimahasiswa`
--
ALTER TABLE `nilaimahasiswa`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `eskul`
--
ALTER TABLE `eskul`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `matakuliah`
--
ALTER TABLE `matakuliah`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `namaeskul`
--
ALTER TABLE `namaeskul`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nilaimahasiswa`
--
ALTER TABLE `nilaimahasiswa`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `prodi`
--
ALTER TABLE `prodi`
  MODIFY `nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

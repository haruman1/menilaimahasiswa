package Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import io.github.cdimascio.dotenv.Dotenv;
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Wiguna
 */
public class KoneksiDB {
    public static Connection koneksi;

    public static Connection configDB() throws SQLException {
        Dotenv dotenv = Dotenv.configure().directory("src/main/java/Config").load();

        try {// koneksi berhasil
            String alamat_url = dotenv.get("DATABASE_URL");
            // String alamat = dotenv.get("DATABASE_URL");
            String username = dotenv.get("user_database_user");
            String password = dotenv.get("user_database_password");
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());// inti koneksi
            koneksi = DriverManager.getConnection(alamat_url, username, password);
            // JOptionPane.showMessageDialog(null, "Koneksi Berhasil");
        } catch (SQLException e) {
            // koneksi error
            JOptionPane.showMessageDialog(null,
                    "Koneksi Gagal karena database belum hidup, dikarenakan : " + e.getMessage());
        }
        return koneksi;

    }

    public static void main(String[] args) throws SQLException {
        Connection HubungkanDB = (Connection) KoneksiDB.configDB();
    }
}
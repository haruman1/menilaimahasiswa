
package Config;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Main.Mahasiswa;
import Main.Menu;
import javax.swing.JOptionPane;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import javax.swing.JOptionPane;

/**
 *
 * @author Wiguna
 */
public class PwValidator {

    public static String validatePW(String password, String username) {
        Argon2 argon2 = Argon2Factory.create(
                Argon2Factory.Argon2Types.ARGON2id,
                16,
                32);
        char[] passwordsaya = password.toCharArray();
        // Buat query untuk mengambil password yang tersimpan di database
        String query = "SELECT username, password FROM users WHERE username = ?";
        String hashPW = argon2.hash(3, // Number of iterations
                64 * 1024, // 64mb
                1, // how many parallel threads to use
                passwordsaya);
        try {

            Connection HubungkanDB = (Connection) KoneksiDB.configDB();
            PreparedStatement stmt = HubungkanDB.prepareStatement(query);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                // Dapatkan password yang tersimpan di database
                String dbPassword = rs.getString("password");
                // Cek apakah password yang dimasukkan sama dengan password yang tersimpan di
                // database

                if (argon2.verify(hashPW, dbPassword)) {
                    JOptionPane.showMessageDialog(null, "Login Berhasil");
                    // new Menu().setVisible(true);
                    Menu menu_user = new Menu();
                    // new History().setVisible(true);
                    // this.dispose();
                    menu_user.setMylabelNama(rs.getString("nama").toUpperCase());
                    menu_user.setMylabelid(rs.getString("id_users").toUpperCase());
                    menu_user.setVisible(true);
                    Mahasiswa awalmhs = new Mahasiswa();
                    awalmhs.setMylabelNama(rs.getString("nama").toUpperCase());
                    awalmhs.setMylabelid(rs.getString("id_users").toUpperCase());
                } else {
                    JOptionPane.showMessageDialog(null, "Username atau Password Salah");
                }

                // // Dapatkan password yang tersimpan di database
                // String dbPassword = rs.getString("password");
                // // Cek apakah password yang dimasukkan sama dengan password yang tersimpan di
                // // database
                // return dbPassword.equals(password);
            } else {
                JOptionPane.showMessageDialog(null, "Maaf database tidak ada");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return null;
    }

    public static String buatpassword(char[] password) {
        Argon2 argon2 = Argon2Factory.create(
                Argon2Factory.Argon2Types.ARGON2id,
                16,
                32);

        String hash = argon2.hash(3, // Number of iterations
                64 * 1024, // 64mb
                1, // how many parallel threads to use
                password);
        return hash;
    }

    public static String puter(String password) {
        Argon2 argon2 = Argon2Factory.create(
                Argon2Factory.Argon2Types.ARGON2id,
                16,
                32);

        char[] passworda = password.toCharArray();
        String hash = argon2.hash(3, // Number of iterations
                64 * 1024, // 64mb
                1, // how many parallel threads to use
                passworda);
        return hash;

    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Main;

import Config.KoneksiDB;
import Authentication.Login;
import Penilaian.Fuzzy;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Wiguna
 */
public class Mahasiswa extends javax.swing.JFrame {

        public void setMylabelNama(String string) {
                labelNama.setText(string);
        }

        public void setMylabelid(String string) {
                labelid.setText(string);
        }

        /**
         * Creates new form Mahasiswa
         */
        public Mahasiswa() {
                initComponents();
                Tampil_Jam();
                Tampil_Tanggal();
                addItemCheckBox();
                createTable();
                clear();

        }

        private void clear() {
                labelNim.setText("");
                labelAddnama.setText("");
                Rdaktif.setSelected(true);
                RdTaat.setSelected(false);
                RdtdkT.setSelected(true);
                RdpTdkA.setSelected(false);
                isianMatakuliah.setSelectedIndex(0);
                isianMK1.setText("");
                isianMK2.setText("");
                isianMK3.setText("");
                isianMK4.setText("");

                // IPK.setText("");
                isianProdi.setSelectedItem("Pilih Prodi");
                btnSimpan.setVisible(true);
                btnEdit.setVisible(false);
                btnHapus.setVisible(false);
                btnBatal.setVisible(false);

        }

        private void createTable() {
                DefaultTableModel table = new DefaultTableModel();
                table.addColumn("NIM");
                table.addColumn("Nama");
                table.addColumn("Prodi");

                table.addColumn("kelas");
                table.addColumn("matakuliah");
                table.addColumn("Kegiatan");
                table.addColumn("Prediksi");
                table.addColumn("IPK Total");
                table.addColumn("Taat Azas");
                table.addColumn("Pencapaian");
                table.addColumn("Dosen Wali");

                // menampilkan data ke dalam table
                try {
                        String sqlQuery = "SELECT mahasiswa.nim, mahasiswa.nama,mahasiswa.prodi, mahasiswa.taat,mahasiswa.kelas, mahasiswa.kegiatan, mahasiswa.prediksi, mahasiswa.yang_nambahin, nilaimahasiswa.pencapaian,nilaimahasiswa.mataKuliah, users.nama_dosen, nilaimahasiswa.ipkTotal FROM mahasiswa INNER JOIN nilaimahasiswa ON  mahasiswa.nim = nilaimahasiswa.nim INNER JOIN users ON mahasiswa.yang_nambahin = users.id_users WHERE mahasiswa.yang_nambahin = '"
                                        + labelid.getText()
                                        + "'";

                        // memanggil koneksi
                        Connection HubungkanDB = (Connection) KoneksiDB.configDB();
                        ResultSet resultTable = HubungkanDB.createStatement().executeQuery(sqlQuery);

                        while (resultTable.next()) {
                                table.addRow(new Object[] {
                                                resultTable.getString("nim"),
                                                resultTable.getString("mahasiswa.nama"),
                                                resultTable.getString("prodi"),
                                                resultTable.getString("kelas"),
                                                resultTable.getString("mataKuliah"),
                                                resultTable.getString("Kegiatan"),
                                                resultTable.getString("prediksi"),
                                                resultTable.getString("ipkTotal"),
                                                resultTable.getString("taat"),
                                                resultTable.getString("pencapaian"),
                                                resultTable.getString("nama_dosen"),
                                });
                        }
                        tableMahasiswa.setModel(table);
                        tableMahasiswa.setDefaultEditor(Object.class, null);
                } catch (SQLException e) {
                        JOptionPane.showMessageDialog(null, "Error karena : " + e.getMessage());
                }
        }

        public void Tampil_Jam() {
                ActionListener taskPerformer = new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent evt) {
                                String nol_jam = "", nol_menit = "", nol_detik = "";

                                java.util.Date dateTime = new java.util.Date();
                                int nilai_jam = dateTime.getHours();
                                int nilai_menit = dateTime.getMinutes();
                                int nilai_detik = dateTime.getSeconds();

                                if (nilai_jam <= 9)
                                        nol_jam = "0";
                                if (nilai_menit <= 9)
                                        nol_menit = "0";
                                if (nilai_detik <= 9)
                                        nol_detik = "0";

                                String jam = nol_jam + Integer.toString(nilai_jam);
                                String menit = nol_menit + Integer.toString(nilai_menit);
                                String detik = nol_detik + Integer.toString(nilai_detik);

                                labelMenit.setText(jam + ":" + menit + ":" + detik + "");
                        }
                };
                new Timer(1000, taskPerformer).start();
        }

        public void Tampil_Tanggal() {
                java.util.Date tglsekarang = new java.util.Date();
                SimpleDateFormat smpdtfmt = new SimpleDateFormat("dd MMMMMMMMM yyyy", Locale.getDefault());
                String tanggal = smpdtfmt.format(tglsekarang);
                labelTanggal.setText(tanggal);
        }

        /**
         * This method is called from within the constructor to initialize the form.
         * WARNING: Do NOT modify this code. The content of this method is always
         * regenerated by the Form Editor.
         */
        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // <editor-fold defaultstate="collapsed" desc="Generated
        // Code">//GEN-BEGIN:initComponents
        private void initComponents() {

                topbar = new javax.swing.JPanel();
                MahasiswaMenu = new javax.swing.JLabel();
                labelWelcome = new javax.swing.JLabel();
                labelid = new javax.swing.JLabel();
                labelNama = new javax.swing.JLabel();
                labelTanggal = new javax.swing.JLabel();
                labelMenit = new javax.swing.JLabel();
                logout = new javax.swing.JButton();
                btnBack = new javax.swing.JButton();
                Main = new javax.swing.JPanel();
                tambahmaha = new javax.swing.JLabel();
                tambahmaha1 = new javax.swing.JLabel();
                jScrollPane1 = new javax.swing.JScrollPane();
                tableMahasiswa = new javax.swing.JTable();
                AddMahasiswa = new javax.swing.JPanel();
                labelNim = new javax.swing.JTextField();
                labelAddnama = new javax.swing.JTextField();
                RdpTdkA = new javax.swing.JRadioButton();
                Rdaktif = new javax.swing.JRadioButton();
                jLabel2 = new javax.swing.JLabel();
                nimLabel = new javax.swing.JLabel();
                namaLabel = new javax.swing.JLabel();
                kelasAdd = new javax.swing.JTextField();
                alamatLabel1 = new javax.swing.JLabel();
                btnBatal = new javax.swing.JButton();
                btnSimpan = new javax.swing.JButton();
                btnHapus = new javax.swing.JButton();
                btnEdit = new javax.swing.JButton();
                prodiLabel = new javax.swing.JLabel();
                isianProdi = new javax.swing.JComboBox<>();
                jLabel3 = new javax.swing.JLabel();
                jLabel4 = new javax.swing.JLabel();
                jLabel5 = new javax.swing.JLabel();
                jLabel6 = new javax.swing.JLabel();
                jLabel7 = new javax.swing.JLabel();
                jLabel8 = new javax.swing.JLabel();
                RdTaat = new javax.swing.JRadioButton();
                RdtdkT = new javax.swing.JRadioButton();
                jLabel10 = new javax.swing.JLabel();
                mataKLabel = new javax.swing.JLabel();
                isianMatakuliah = new javax.swing.JComboBox<>();
                p1Label = new javax.swing.JLabel();
                isianMK1 = new javax.swing.JTextField();
                p2Label = new javax.swing.JLabel();
                isianMK2 = new javax.swing.JTextField();
                p3Label = new javax.swing.JLabel();
                isianMK3 = new javax.swing.JTextField();
                p4Label = new javax.swing.JLabel();
                isianMK4 = new javax.swing.JTextField();
                btnRefresh = new javax.swing.JButton();

                setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                setResizable(false);

                MahasiswaMenu.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
                MahasiswaMenu.setText("Menu Mahasiswa");

                labelWelcome.setText("Selamat Datang");

                labelid.setText("ID users");

                labelNama.setText("Nama");

                labelTanggal.setText("Waktu");

                labelMenit.setText("Waktu");

                logout.setBackground(new java.awt.Color(204, 0, 0));
                logout.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
                logout.setForeground(new java.awt.Color(255, 255, 255));
                logout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/logout.png"))); // NOI18N
                logout.setText("Logout");
                logout.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                logoutActionPerformed(evt);
                        }
                });

                btnBack.setBackground(new java.awt.Color(255, 0, 102));
                btnBack.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
                btnBack.setForeground(new java.awt.Color(255, 255, 255));
                btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/history.png"))); // NOI18N
                btnBack.setText("Kembali");
                btnBack.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnBackActionPerformed(evt);
                        }
                });

                javax.swing.GroupLayout topbarLayout = new javax.swing.GroupLayout(topbar);
                topbar.setLayout(topbarLayout);
                topbarLayout.setHorizontalGroup(
                                topbarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, topbarLayout
                                                                .createSequentialGroup()
                                                                .addContainerGap(427, Short.MAX_VALUE)
                                                                .addComponent(MahasiswaMenu,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                254,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(236, 236, 236)
                                                                .addComponent(btnBack)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(logout)
                                                                .addGap(54, 54, 54))
                                                .addGroup(topbarLayout.createSequentialGroup()
                                                                .addGap(51, 51, 51)
                                                                .addGroup(topbarLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.TRAILING,
                                                                                false)
                                                                                .addComponent(labelid,
                                                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                Short.MAX_VALUE)
                                                                                .addComponent(labelWelcome,
                                                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                89,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(labelNama,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                94,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                Short.MAX_VALUE)
                                                                .addComponent(labelTanggal,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                104,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(labelMenit,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                91,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addContainerGap()));
                topbarLayout.setVerticalGroup(
                                topbarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(topbarLayout.createSequentialGroup()
                                                                .addGroup(topbarLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addGroup(topbarLayout
                                                                                                .createSequentialGroup()
                                                                                                .addGap(29, 29, 29)
                                                                                                .addGroup(topbarLayout
                                                                                                                .createParallelGroup(
                                                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                                                .addComponent(logout)
                                                                                                                .addComponent(btnBack)))
                                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                                                topbarLayout.createSequentialGroup()
                                                                                                                .addContainerGap()
                                                                                                                .addComponent(MahasiswaMenu)))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(topbarLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(labelNama)
                                                                                .addComponent(labelTanggal)
                                                                                .addComponent(labelMenit)
                                                                                .addComponent(labelWelcome))
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(labelid)
                                                                .addContainerGap(22, Short.MAX_VALUE)));

                tambahmaha.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
                tambahmaha.setText("Isian Mahasiswa");

                tambahmaha1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
                tambahmaha1.setText("List Mahasiswa");

                tableMahasiswa.setModel(new javax.swing.table.DefaultTableModel(
                                new Object[][] {
                                                { null, null, null, null, null, null, null, null, null, null, null },
                                                { null, null, null, null, null, null, null, null, null, null, null },
                                                { null, null, null, null, null, null, null, null, null, null, null },
                                                { null, null, null, null, null, null, null, null, null, null, null }
                                },
                                new String[] {
                                                "NIM", "Nama", "Prodi", "kelas", "Kegiatan", "Prediksi", "IPK Total",
                                                "Taat Azas", "Pencapaian", "Mata Kuliah", "Dosen Wali"
                                }) {
                        boolean[] canEdit = new boolean[] {
                                        false, false, false, false, false, false, false, false, false, false, false
                        };

                        public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit[columnIndex];
                        }
                });
                tableMahasiswa.addMouseListener(new java.awt.event.MouseAdapter() {
                        public void mouseClicked(java.awt.event.MouseEvent evt) {
                                tableMahasiswaMouseClicked(evt);
                        }
                });
                jScrollPane1.setViewportView(tableMahasiswa);

                labelNim.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                labelNimActionPerformed(evt);
                        }
                });

                labelAddnama.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                labelAddnamaActionPerformed(evt);
                        }
                });

                RdpTdkA.setText("Tidak Aktif");

                Rdaktif.setSelected(true);
                Rdaktif.setText("Aktif");
                Rdaktif.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                RdaktifActionPerformed(evt);
                        }
                });

                jLabel2.setText("Mahasiswa ini ");

                nimLabel.setText("NIM ");

                namaLabel.setText("Nama");

                alamatLabel1.setText("Kelas");

                btnBatal.setBackground(new java.awt.Color(51, 51, 255));
                btnBatal.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
                btnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/bersihkan.png"))); // NOI18N
                btnBatal.setText("Batal");
                btnBatal.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnBatalActionPerformed(evt);
                        }
                });

                btnSimpan.setBackground(new java.awt.Color(0, 204, 0));
                btnSimpan.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
                btnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/add.png"))); // NOI18N
                btnSimpan.setText("Tambah");
                btnSimpan.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnSimpanActionPerformed(evt);
                        }
                });

                btnHapus.setBackground(new java.awt.Color(255, 0, 51));
                btnHapus.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
                btnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/trash.png"))); // NOI18N
                btnHapus.setText("Hapus");
                btnHapus.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnHapusActionPerformed(evt);
                        }
                });

                btnEdit.setBackground(new java.awt.Color(0, 204, 0));
                btnEdit.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
                btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/update.png"))); // NOI18N
                btnEdit.setText("Simpan");
                btnEdit.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnEditActionPerformed(evt);
                        }
                });

                prodiLabel.setText("Prodi");

                isianProdi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih Prodi" }));

                jLabel3.setText(":");

                jLabel4.setText(":");

                jLabel5.setText(":");

                jLabel6.setText(":");

                jLabel7.setText("Mahasiswa ini ");

                jLabel8.setText(":");

                RdTaat.setSelected(true);
                RdTaat.setText("Taat");
                RdTaat.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                RdTaatActionPerformed(evt);
                        }
                });

                RdtdkT.setText("Tidak Taat");

                jLabel10.setText(":");

                mataKLabel.setText("mata Kuliah");

                isianMatakuliah.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pilih mata Kuliah" }));
                isianMatakuliah.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                isianMatakuliahActionPerformed(evt);
                        }
                });

                p1Label.setText("Nilai Pertemuan 1");

                isianMK1.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                isianMK1ActionPerformed(evt);
                        }
                });

                p2Label.setText("Nilai Pertemuan 2");

                isianMK2.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                isianMK2ActionPerformed(evt);
                        }
                });

                p3Label.setText("Nilai Pertemuan 3");

                isianMK3.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                isianMK3ActionPerformed(evt);
                        }
                });

                p4Label.setText("Nilai Pertemuan 4");

                isianMK4.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                isianMK4ActionPerformed(evt);
                        }
                });

                javax.swing.GroupLayout AddMahasiswaLayout = new javax.swing.GroupLayout(AddMahasiswa);
                AddMahasiswa.setLayout(AddMahasiswaLayout);
                AddMahasiswaLayout.setHorizontalGroup(
                                AddMahasiswaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(AddMahasiswaLayout.createSequentialGroup()
                                                                .addContainerGap()
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addGroup(AddMahasiswaLayout
                                                                                                .createSequentialGroup()
                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                .createParallelGroup(
                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                .createParallelGroup(
                                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                                                                                                AddMahasiswaLayout
                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                .addGap(126, 126,
                                                                                                                                                                                126)
                                                                                                                                                                .addComponent(Rdaktif)
                                                                                                                                                                .addGap(18, 18, 18)
                                                                                                                                                                .addComponent(RdpTdkA))
                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                .createSequentialGroup()
                                                                                                                                                .addComponent(jLabel2)
                                                                                                                                                .addPreferredGap(
                                                                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                                                                                                .addComponent(jLabel5,
                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                10,
                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                .addGap(173, 173,
                                                                                                                                                                173)))
                                                                                                                .addComponent(mataKLabel,
                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                71,
                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                                .addContainerGap(
                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                Short.MAX_VALUE))
                                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                                                AddMahasiswaLayout
                                                                                                                .createSequentialGroup()
                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                .createParallelGroup(
                                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                                                                                                AddMahasiswaLayout
                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                .addGap(126, 126,
                                                                                                                                                                                126)
                                                                                                                                                                .addComponent(RdTaat)
                                                                                                                                                                .addGap(18, 18, 18)
                                                                                                                                                                .addComponent(RdtdkT))
                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                .createSequentialGroup()
                                                                                                                                                .addComponent(jLabel7)
                                                                                                                                                .addPreferredGap(
                                                                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                                                                                                .addComponent(jLabel8,
                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                10,
                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                .addGap(169, 169,
                                                                                                                                                                169)))
                                                                                                                .addGap(46, 46, 46))
                                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                                                AddMahasiswaLayout
                                                                                                                .createSequentialGroup()
                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                .createParallelGroup(
                                                                                                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                .createSequentialGroup()
                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                .createParallelGroup(
                                                                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                                .addGap(0, 0, Short.MAX_VALUE)
                                                                                                                                                                                .addComponent(jLabel6,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                10,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                                .addComponent(namaLabel,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                49,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                                .addPreferredGap(
                                                                                                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                                                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                Short.MAX_VALUE)
                                                                                                                                                                                .addComponent(jLabel3,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                10,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                                .addComponent(prodiLabel,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                49,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                                .addPreferredGap(
                                                                                                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                                                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                Short.MAX_VALUE)
                                                                                                                                                                                .addComponent(jLabel4,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                10,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                                                                                                .addComponent(alamatLabel1,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                37,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                .addComponent(nimLabel,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                49,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                                                                                .addGap(55, 55, 55)
                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                .createParallelGroup(
                                                                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                                                                                .addComponent(labelNim,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                157,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                                .createParallelGroup(
                                                                                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                                                                                                                false)
                                                                                                                                                                                .addComponent(isianProdi,
                                                                                                                                                                                                0,
                                                                                                                                                                                                157,
                                                                                                                                                                                                Short.MAX_VALUE)
                                                                                                                                                                                .addComponent(labelAddnama))))
                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                .createSequentialGroup()
                                                                                                                                                .addGap(89, 89, 89)
                                                                                                                                                .addComponent(jLabel10,
                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                10,
                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                .addGap(21, 21, 21)
                                                                                                                                                .addComponent(kelasAdd,
                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                157,
                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                                                                AddMahasiswaLayout
                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                .addComponent(p4Label,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                99,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                .addGap(18, 18, 18)
                                                                                                                                                                .addComponent(isianMK4))
                                                                                                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                                                                AddMahasiswaLayout
                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                .addComponent(p2Label,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                99,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                .addGap(18, 18, 18)
                                                                                                                                                                .addComponent(isianMK2))
                                                                                                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                                                                AddMahasiswaLayout
                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                .addComponent(p1Label,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                99,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                .addGap(18, 18, 18)
                                                                                                                                                                .addComponent(isianMK1))
                                                                                                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                                                                AddMahasiswaLayout
                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                .addComponent(p3Label,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                99,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                .addGap(18, 18, 18)
                                                                                                                                                                .addComponent(isianMK3))
                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                .createSequentialGroup()
                                                                                                                                                .addGap(0, 0, Short.MAX_VALUE)
                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                .createParallelGroup(
                                                                                                                                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                                                                                                .addComponent(isianMatakuliah,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                157,
                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                                .createParallelGroup(
                                                                                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                                                                                                                                                false)
                                                                                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                                                                                .createSequentialGroup()
                                                                                                                                                                                                .addComponent(btnEdit,
                                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                                106,
                                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                                                                                                .addPreferredGap(
                                                                                                                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                                                                                                                                                .addComponent(btnHapus,
                                                                                                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                                Short.MAX_VALUE))
                                                                                                                                                                                .addComponent(btnSimpan,
                                                                                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                                                                Short.MAX_VALUE)
                                                                                                                                                                                .addComponent(btnBatal,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                                                                214,
                                                                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                                                                                                .addGap(37, 37, 37)))));
                AddMahasiswaLayout.setVerticalGroup(
                                AddMahasiswaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AddMahasiswaLayout
                                                                .createSequentialGroup()
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addGroup(AddMahasiswaLayout
                                                                                                .createSequentialGroup()
                                                                                                .addGap(21, 21, 21)
                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                .createParallelGroup(
                                                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                                                .addComponent(nimLabel)
                                                                                                                .addComponent(jLabel6)
                                                                                                                .addComponent(labelNim,
                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                                .addPreferredGap(
                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                .createParallelGroup(
                                                                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                                                .addComponent(labelAddnama,
                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                31,
                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                                .addComponent(namaLabel)
                                                                                                                .addComponent(jLabel3))
                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                .createParallelGroup(
                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                .createSequentialGroup()
                                                                                                                                .addPreferredGap(
                                                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                                                                .addComponent(prodiLabel))
                                                                                                                .addGroup(AddMahasiswaLayout
                                                                                                                                .createSequentialGroup()
                                                                                                                                .addGap(3, 3, 3)
                                                                                                                                .addComponent(isianProdi,
                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                                                AddMahasiswaLayout
                                                                                                                .createSequentialGroup()
                                                                                                                .addContainerGap()
                                                                                                                .addComponent(jLabel4)))
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(kelasAdd,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addComponent(jLabel10)
                                                                                .addComponent(alamatLabel1))
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(jLabel2)
                                                                                .addComponent(Rdaktif)
                                                                                .addComponent(RdpTdkA)
                                                                                .addComponent(jLabel5))
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(jLabel7)
                                                                                .addComponent(RdTaat)
                                                                                .addComponent(RdtdkT)
                                                                                .addComponent(jLabel8))
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(mataKLabel)
                                                                                .addComponent(isianMatakuliah,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(p1Label)
                                                                                .addComponent(isianMK1,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(p2Label)
                                                                                .addComponent(isianMK2,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(p3Label)
                                                                                .addComponent(isianMK3,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addComponent(p4Label)
                                                                                .addComponent(isianMK4,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(18, 18, 18)
                                                                .addComponent(btnSimpan)
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(AddMahasiswaLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                .addComponent(btnEdit)
                                                                                .addComponent(btnHapus))
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(btnBatal)
                                                                .addContainerGap(272, Short.MAX_VALUE)));

                btnRefresh.setText("Refresh Tabel");
                btnRefresh.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(java.awt.event.ActionEvent evt) {
                                btnRefreshActionPerformed(evt);
                        }
                });

                javax.swing.GroupLayout MainLayout = new javax.swing.GroupLayout(Main);
                Main.setLayout(MainLayout);
                MainLayout.setHorizontalGroup(
                                MainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(MainLayout.createSequentialGroup()
                                                                .addGroup(MainLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addGroup(MainLayout
                                                                                                .createSequentialGroup()
                                                                                                .addGap(76, 76, 76)
                                                                                                .addComponent(tambahmaha,
                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                246,
                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                .addGroup(MainLayout
                                                                                                .createSequentialGroup()
                                                                                                .addContainerGap()
                                                                                                .addComponent(AddMahasiswa,
                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGroup(MainLayout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addGroup(MainLayout
                                                                                                .createSequentialGroup()
                                                                                                .addGap(353, 353, 353)
                                                                                                .addComponent(tambahmaha1))
                                                                                .addGroup(MainLayout
                                                                                                .createSequentialGroup()
                                                                                                .addPreferredGap(
                                                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                                .addGroup(MainLayout
                                                                                                                .createParallelGroup(
                                                                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                                .addComponent(jScrollPane1,
                                                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                                                745,
                                                                                                                                Short.MAX_VALUE)
                                                                                                                .addGroup(MainLayout
                                                                                                                                .createSequentialGroup()
                                                                                                                                .addGap(590, 590,
                                                                                                                                                590)
                                                                                                                                .addComponent(btnRefresh,
                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                                                                144,
                                                                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                                                .addContainerGap()));
                MainLayout.setVerticalGroup(
                                MainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(MainLayout.createSequentialGroup()
                                                                .addContainerGap()
                                                                .addComponent(tambahmaha)
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                                                27, Short.MAX_VALUE)
                                                                .addComponent(AddMahasiswa,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addContainerGap())
                                                .addGroup(MainLayout.createSequentialGroup()
                                                                .addGap(34, 34, 34)
                                                                .addComponent(tambahmaha1)
                                                                .addGap(27, 27, 27)
                                                                .addComponent(btnRefresh)
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jScrollPane1,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                487,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                Short.MAX_VALUE)));

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                                .addContainerGap()
                                                                .addGroup(layout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addComponent(Main,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addComponent(topbar,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addContainerGap(46, Short.MAX_VALUE)));
                layout.setVerticalGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                                .addGap(20, 20, 20)
                                                                .addComponent(topbar,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                Short.MAX_VALUE)
                                                                .addComponent(Main,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addContainerGap()));

                pack();
        }// </editor-fold>//GEN-END:initComponents

        private void isianMK4ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_isianMK4ActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_isianMK4ActionPerformed

        private void isianMK3ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_isianMK3ActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_isianMK3ActionPerformed

        private void isianMK2ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_isianMK2ActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_isianMK2ActionPerformed

        private void isianMK1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_isianMK1ActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_isianMK1ActionPerformed

        private void isianMatakuliahActionPerformed(java.awt.event.ActionEvent evt) {
                // GEN-FIRST:event_isianMatakuliahActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_isianMatakuliahActionPerformed

        private void RdTaatActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_RdTaatActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_RdTaatActionPerformed

        private void RdLActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_RdLActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_RdLActionPerformed

        private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnBackActionPerformed
                int option = JOptionPane.showConfirmDialog(null, "Apakah Anda yakin untuk kembali?", "Perhatian",
                                JOptionPane.YES_NO_OPTION);
                if (option == 0) {
                        Menu menu_user = new Menu();
                        menu_user.setMylabelNama(labelNama.getText());
                        menu_user.setMylabelid(labelid.getText());
                        menu_user.setVisible(true);
                        this.dispose();
                } else {

                }
        }// GEN-LAST:event_btnBackActionPerformed

        private void RdaktifActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_RdaktifActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_RdaktifActionPerformed

        private void labelAddnamaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_labelAddnamaActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_labelAddnamaActionPerformed

        private void labelNimActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_labelNimActionPerformed
                // TODO add your handling code here:
        }// GEN-LAST:event_labelNimActionPerformed

        private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnRefreshActionPerformed
                JOptionPane.showMessageDialog(null, "Tabel berhasil di refresh");
                createTable();
                clear();
        }// GEN-LAST:event_btnRefreshActionPerformed

        private void addItemCheckBox() {
                try {
                        String sql = "S"
                                        + "ELECT namaProdi FROM prodi";
                        Connection HubungkanDB = (Connection) KoneksiDB.configDB();
                        ResultSet resultProdi = HubungkanDB.createStatement().executeQuery(sql);
                        while (resultProdi.next()) {

                                isianProdi.addItem(resultProdi.getString("namaProdi"));

                        }
                        resultProdi.close();
                        String matakuliah = "SELECT namaMatkul FROM matakuliah";
                        ResultSet resultMatkul = HubungkanDB.createStatement().executeQuery(matakuliah);
                        while (resultMatkul.next()) {
                                if (isianMatakuliah.getSelectedIndex() > 0) {
                                        isianMatakuliah.addItem(resultMatkul.getString("namaMatkul"));
                                }
                        }
                        resultMatkul.close();
                } catch (HeadlessException |

                                SQLException e) {
                        JOptionPane.showMessageDialog(null, e);
                }
        }

        private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnSimpanActionPerformed
                // TODO add your handling code here:
                try {
                        String nim = labelNim.getText();
                        String nama = labelAddnama.getText();
                        String Prodi = isianProdi.getSelectedItem().toString();
                        String taat;
                        String aktifKegiatan;
                        String taatkegiatan;
                        String nilaiMatkul = "";
                        String mahasiswaBerprestasi = "";
                        String totalIPK = "";
                        if (Rdaktif.isSelected()) {
                                aktifKegiatan = "Aktif";
                        } else {
                                aktifKegiatan = "Tidak aktif";
                        }
                        if (RdTaat.isSelected()) {
                                taatkegiatan = "Taat";
                        } else {
                                taatkegiatan = "Tidak taat";
                        }
                        String matkulMahasiswa = isianMatakuliah.getSelectedItem().toString();

                        Integer matkul1 = Integer.parseInt(isianMK1.getText()) / 4;
                        Integer matkul2 = Integer.parseInt(isianMK2.getText()) / 4;
                        Integer matkul3 = Integer.parseInt(isianMK3.getText()) / 4;
                        Integer matkul4 = Integer.parseInt(isianMK4.getText()) / 4;
                        Integer NilaiMataKuliah = matkul1 + matkul2 + matkul3 + matkul4;
                        String kelas = kelasAdd.getText();
                        // String IPKadd = IPK.getText();
                        if ("Aktif".equals(aktifKegiatan) && "Taat".equals(taatkegiatan)) {
                                if (NilaiMataKuliah >= 85) {
                                        totalIPK = "4.00";
                                        mahasiswaBerprestasi = "Berprestasi";
                                        nilaiMatkul = "A";
                                } else if (NilaiMataKuliah >= 80) {
                                        totalIPK = "3.75";
                                        mahasiswaBerprestasi = "Berprestasi";
                                        nilaiMatkul = "AB";
                                } else if (NilaiMataKuliah >= 75) {
                                        totalIPK = "3.50";
                                        mahasiswaBerprestasi = "Berprestasi";
                                        nilaiMatkul = "B";

                                } else if (NilaiMataKuliah >= 70) {
                                        totalIPK = "3.25";
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        nilaiMatkul = "BC";
                                } else if (NilaiMataKuliah >= 65) {
                                        totalIPK = "3.00";
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        nilaiMatkul = "C";
                                }
                        } else if (aktifKegiatan == "Aktif" && taatkegiatan == "Tidak taat") {
                                if (NilaiMataKuliah >= 85) {
                                        mahasiswaBerprestasi = "Berprestasi";
                                        totalIPK = "3.75";
                                        nilaiMatkul = "A";
                                } else if (NilaiMataKuliah >= 80) {
                                        mahasiswaBerprestasi = "Berprestasi";
                                        totalIPK = "3.50";
                                        nilaiMatkul = "AB";
                                } else if (NilaiMataKuliah >= 75) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.25";
                                        nilaiMatkul = "B";
                                } else if (NilaiMataKuliah >= 70) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.00";
                                        nilaiMatkul = "BC";
                                } else if (NilaiMataKuliah >= 65) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.75";
                                        nilaiMatkul = "C";
                                }
                        } else if (aktifKegiatan == "Tidak aktif" && taatkegiatan == "Tidak taat") {
                                if (NilaiMataKuliah >= 85) {
                                        mahasiswaBerprestasi = "Berprestasi";
                                        totalIPK = "3.50";
                                        nilaiMatkul = "AB";
                                } else if (NilaiMataKuliah >= 80) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.25";
                                        nilaiMatkul = "B";
                                } else if (NilaiMataKuliah >= 75) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.00";
                                        nilaiMatkul = "C";
                                } else if (NilaiMataKuliah >= 70) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.75";
                                        nilaiMatkul = "CD";
                                } else if (NilaiMataKuliah >= 65) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.50";
                                        nilaiMatkul = "D";
                                }
                        } else {
                                if (NilaiMataKuliah >= 85) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.25";
                                        nilaiMatkul = "B";
                                } else if (NilaiMataKuliah >= 80) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.00";
                                        nilaiMatkul = "BC";
                                } else if (NilaiMataKuliah >= 75) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.75";
                                        nilaiMatkul = "C";
                                } else if (NilaiMataKuliah >= 70) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.50";
                                        nilaiMatkul = "CD";
                                } else if (NilaiMataKuliah >= 65) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.25";
                                        nilaiMatkul = "D";
                                }
                        }

                        Connection conn = (Connection) KoneksiDB.configDB();
                        String queryMahasiswa = "INSERT INTO mahasiswa (nim, nama, prodi, kelas, taat, kegiatan, ipk, prediksi, yang_nambahin)"
                                        + "VALUES( '"
                                        + nim
                                        + "', '"
                                        + nama
                                        + "','"
                                        + Prodi
                                        + "', '"
                                        + kelas
                                        + "', '"
                                        + taatkegiatan +
                                        "', '"
                                        + aktifKegiatan +
                                        "', '"
                                        + totalIPK
                                        + "', '"
                                        + mahasiswaBerprestasi
                                        + "', '"
                                        + labelid.getText() +
                                        "')";
                        PreparedStatement newData = conn.prepareStatement(queryMahasiswa);
                        newData.execute();
                        String queryNilai = "INSERT INTO nilaimahasiswa (nim, nilai1, nilai2, nilai3, nilai4, nilaiTotal, pencapaian, mataKuliah, ipkTotal)"
                                        + "VALUES( '" + nim + "', '" + matkul1
                                        + "','" + matkul2
                                        + "', '"
                                        + matkul3 + "', '" + matkul4 + "', '" + NilaiMataKuliah + "', '" + nilaiMatkul
                                        + "', '"
                                        + matkulMahasiswa
                                        + "', '"
                                        + totalIPK + "')";
                        PreparedStatement newDataNilai = conn.prepareStatement(queryNilai);
                        newDataNilai.execute();
                        createTable();

                        clear();

                } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                }

                // memanggil koneksi

        }// GEN-LAST:event_btnSimpanActionPerformed

        private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnEditActionPerformed
                // TODO add your handling code here:
                try {
                        String nim = labelNim.getText();
                        String nama = labelAddnama.getText();
                        String Prodi = isianProdi.getSelectedItem().toString();
                        String taat;
                        String aktifKegiatan;
                        String taatkegiatan;
                        String nilaiMatkul = "";
                        String mahasiswaBerprestasi = "";
                        String totalIPK = "";
                        if (Rdaktif.isSelected()) {
                                aktifKegiatan = "Aktif";
                        } else {
                                aktifKegiatan = "Tidak aktif";
                        }
                        if (RdTaat.isSelected()) {
                                taatkegiatan = "Taat";
                        } else {
                                taatkegiatan = "Tidak taat";
                        }
                        String matkulMahasiswa = isianMatakuliah.getSelectedItem().toString();

                        Integer matkul1 = Integer.parseInt(isianMK1.getText()) / 4;
                        Integer matkul2 = Integer.parseInt(isianMK2.getText()) / 4;
                        Integer matkul3 = Integer.parseInt(isianMK3.getText()) / 4;
                        Integer matkul4 = Integer.parseInt(isianMK4.getText()) / 4;
                        Integer NilaiMataKuliah = matkul1 + matkul2 + matkul3 + matkul4;
                        String kelas = kelasAdd.getText();
                        // String IPKadd = IPK.getText();
                        if ("Aktif".equals(aktifKegiatan) && "Taat".equals(taatkegiatan)) {
                                if (NilaiMataKuliah >= 85) {
                                        totalIPK = "4.00";
                                        mahasiswaBerprestasi = "Berprestasi";
                                        nilaiMatkul = "A";
                                } else if (NilaiMataKuliah >= 80) {
                                        totalIPK = "3.75";
                                        mahasiswaBerprestasi = "Berprestasi";
                                        nilaiMatkul = "AB";
                                } else if (NilaiMataKuliah >= 75) {
                                        totalIPK = "3.50";
                                        mahasiswaBerprestasi = "Berprestasi";
                                        nilaiMatkul = "B";

                                } else if (NilaiMataKuliah >= 70) {
                                        totalIPK = "3.25";
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        nilaiMatkul = "BC";
                                } else if (NilaiMataKuliah >= 65) {
                                        totalIPK = "3.00";
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        nilaiMatkul = "C";
                                }
                        } else if (aktifKegiatan == "Aktif" && taatkegiatan == "Tidak taat") {
                                if (NilaiMataKuliah >= 85) {
                                        mahasiswaBerprestasi = "Berprestasi";
                                        totalIPK = "3.75";
                                        nilaiMatkul = "A";
                                } else if (NilaiMataKuliah >= 80) {
                                        mahasiswaBerprestasi = "Berprestasi";
                                        totalIPK = "3.50";
                                        nilaiMatkul = "AB";
                                } else if (NilaiMataKuliah >= 75) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.25";
                                        nilaiMatkul = "B";
                                } else if (NilaiMataKuliah >= 70) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.00";
                                        nilaiMatkul = "BC";
                                } else if (NilaiMataKuliah >= 65) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.75";
                                        nilaiMatkul = "C";
                                }
                        } else if (aktifKegiatan == "Tidak aktif" && taatkegiatan == "Tidak taat") {
                                if (NilaiMataKuliah >= 85) {
                                        mahasiswaBerprestasi = "Berprestasi";
                                        totalIPK = "3.50";
                                        nilaiMatkul = "AB";
                                } else if (NilaiMataKuliah >= 80) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.25";
                                        nilaiMatkul = "B";
                                } else if (NilaiMataKuliah >= 75) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.00";
                                        nilaiMatkul = "C";
                                } else if (NilaiMataKuliah >= 70) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.75";
                                        nilaiMatkul = "CD";
                                } else if (NilaiMataKuliah >= 65) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.50";
                                        nilaiMatkul = "D";
                                }
                        } else {
                                if (NilaiMataKuliah >= 85) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.25";
                                        nilaiMatkul = "B";
                                } else if (NilaiMataKuliah >= 80) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "3.00";
                                        nilaiMatkul = "BC";
                                } else if (NilaiMataKuliah >= 75) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.75";
                                        nilaiMatkul = "C";
                                } else if (NilaiMataKuliah >= 70) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.50";
                                        nilaiMatkul = "CD";
                                } else if (NilaiMataKuliah >= 65) {
                                        mahasiswaBerprestasi = "Tidak berprestasi";
                                        totalIPK = "2.25";
                                        nilaiMatkul = "D";
                                }
                        }

                        Connection conn = (Connection) KoneksiDB.configDB();

                        String query = "UPDATE mahasiswa set nim = '" + nim + "', "
                                        + "nama = '" + nama + "', "
                                        + "prodi = '" + Prodi + "', "
                                        + "taat = '" + taatkegiatan + "', "
                                        + "kegiatan = '" + kelas + "', "
                                        + "taat = '" + taatkegiatan + "', "
                                        + "kegiatan = '" + aktifKegiatan + "', "

                                        // + "ipk = '" + IPKadd + "' , "
                                        + "ipk = '" + totalIPK + "', "
                                        + "prediksi = '" + mahasiswaBerprestasi + "' "

                                        + "WHERE nim = '" + nim + "'";
                        PreparedStatement newData = conn.prepareStatement(query);
                        newData.execute();
                        String queryNilai = "UPDATE nilaimahasiswa set nim = '" + nim + "', "
                                        + "nilai1 = '" + matkul1 + "', "
                                        + "nilai2 = '" + matkul2 + "', "
                                        + "nilai3 = '" + matkul3 + "', "
                                        + "nilai4 = '" + matkul4 + "', "
                                        + "nilaiTotal = '" + NilaiMataKuliah + "', "
                                        + "pencapaian = '" + nilaiMatkul + "', "

                                        // + "ipk = '" + IPKadd + "' , "
                                        + "mataKuliah = '" + matkulMahasiswa + "', "
                                        + "ipkTotal = '" + totalIPK + "' "

                                        + "WHERE nim = '" + nim + "'";
                        PreparedStatement newDataNilai = conn.prepareStatement(queryNilai);
                        newDataNilai.execute();

                        JOptionPane.showMessageDialog(null, "Edit Mahasiswa berhasil");
                } catch (HeadlessException | SQLException e) {
                        JOptionPane.showMessageDialog(this, e.getMessage());
                }
                createTable();
                clear();
        }// GEN-LAST:event_btnEditActionPerformed

        private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnHapusActionPerformed
                // TODO add your handling code here:
                Integer isConfirm = JOptionPane.showConfirmDialog(null,
                                "Apakah anda yakin akan menghapus Mahasiswa Tersebut?",
                                "Delete data", JOptionPane.YES_NO_OPTION);

                if (isConfirm != JOptionPane.YES_OPTION) {
                        return;
                }

                try {
                        String nim = labelNim.getText();
                        String nama = labelAddnama.getText();
                        String Prodi = isianProdi.getSelectedItem().toString();
                        // String IPKadd = IPK.getText();
                        // memanggil koneksi
                        Connection conn = (Connection) KoneksiDB.configDB();

                        String queryhapusMahasiswa = "DELETE FROM mahasiswa WHERE nim = '" + nim + "' AND nama = '"
                                        + nama + "' AND prodi = '" + Prodi + "'";

                        PreparedStatement newDatahaous = conn.prepareStatement(queryhapusMahasiswa);
                        newDatahaous.execute();
                        String matkul1 = isianMK1.getText();
                        String matkul2 = isianMK2.getText();
                        String matkul3 = isianMK3.getText();
                        String matkul4 = isianMK4.getText();

                        String queryhapusNilai = "DELETE FROM nilaimahasiswa WHERE nim = '" + nim + "' AND nilai1 = '"
                                        + matkul1 + "' AND nilai2 = '" + matkul2 + "' AND nilai3 = '" + matkul3
                                        + "'AND nilai4 = '" + matkul4 + "'";

                        PreparedStatement newDataNilai = conn.prepareStatement(queryhapusNilai);
                        newDataNilai.execute();

                        JOptionPane.showMessageDialog(null, "Hapus Mahasiswa berhasil");
                } catch (HeadlessException | SQLException e) {
                        JOptionPane.showMessageDialog(this, e.getMessage());
                }
                createTable();
                clear();
        }// GEN-LAST:event_btnHapusActionPerformed

        private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnBatalActionPerformed
                // TODO add your handling code here:
                clear();
        }// GEN-LAST:event_btnBatalActionPerformed

        private void tableMahasiswaMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_tableMahasiswaMouseClicked
                // TODO add your handling code here:
                Integer row = tableMahasiswa.rowAtPoint(evt.getPoint());
                String NIM = tableMahasiswa.getValueAt(row, 0).toString();
                String Nama = tableMahasiswa.getValueAt(row, 1).toString();
                String Prodi = tableMahasiswa.getValueAt(row, 2).toString();
                String Kelas = tableMahasiswa.getValueAt(row, 3).toString();
                String Kegiatan = tableMahasiswa.getValueAt(row, 4).toString();
                String begitusulitLupakanreyhan = tableMahasiswa.getValueAt(row, 5).toString();
                String TaatAzas = tableMahasiswa.getValueAt(row, 6).toString();
                // isianMatakuliah.setSelectedItem(mataKuliah);
                isianMatakuliah.setSelectedItem(begitusulitLupakanreyhan);
                labelNim.setText(NIM);
                labelAddnama.setText(Nama);
                isianProdi.setSelectedItem(Prodi);
                kelasAdd.setText(Kelas);
                if (Kegiatan == "Aktif") {
                        Rdaktif.setSelected(true);
                        RdpTdkA.setSelected(false);
                } else {
                        Rdaktif.setSelected(false);
                        RdpTdkA.setSelected(true);
                }

                if (TaatAzas == "Taat") {
                        RdTaat.setSelected(true);
                        RdtdkT.setSelected(false);
                } else {
                        RdtdkT.setSelected(true);
                        RdTaat.setSelected(false);
                }
                String dapetinNilaiMahasiswa = "SELECT * FROM nilaimahasiswa WHERE nim = '" + NIM + "' ";
                try {
                        Connection conn = (Connection) KoneksiDB.configDB();
                        Statement st = conn.createStatement();
                        ResultSet rs = st.executeQuery(dapetinNilaiMahasiswa);
                        while (rs.next()) {

                                isianMK1.setText(String.valueOf(Integer.parseInt(rs.getString("nilai1")) * 4));
                                isianMK2.setText(String.valueOf(Integer.parseInt(rs.getString("nilai2")) * 4));
                                isianMK3.setText(String.valueOf(Integer.parseInt(rs.getString("nilai3")) * 4));
                                isianMK4.setText(String.valueOf(Integer.parseInt(rs.getString("nilai4")) * 4));

                        }
                } catch (SQLException e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                }
                // IPK.setText(IPK_set);

                btnSimpan.setVisible(false);
                btnEdit.setVisible(true);
                btnHapus.setVisible(true);
                btnBatal.setVisible(true);
        }// GEN-LAST:event_tableMahasiswaMouseClicked

        private void logoutActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_logoutActionPerformed
                // TODO add your handling code here:
                int option = JOptionPane.showConfirmDialog(null, "Apakah Anda yakin untuk keluar?", "Perhatian",
                                JOptionPane.YES_NO_OPTION);
                if (option == 0) {
                        new Login().setVisible(true);
                        this.dispose();
                } else {

                }
        }// GEN-LAST:event_logoutActionPerformed

        /**
         * @param args the command line arguments
         */
        public static void main(String args[]) {
                /* Set the Nimbus look and feel */
                // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
                // (optional) ">
                /*
                 * If Nimbus (introduced in Java SE 6) is not available, stay with the default
                 * look and feel.
                 * For details see
                 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
                 */
                try {
                        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
                                        .getInstalledLookAndFeels()) {
                                if ("Nimbus".equals(info.getName())) {
                                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                                        break;
                                }
                        }
                } catch (ClassNotFoundException ex) {
                        java.util.logging.Logger.getLogger(Mahasiswa.class.getName())
                                        .log(java.util.logging.Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                        java.util.logging.Logger.getLogger(Mahasiswa.class.getName())
                                        .log(java.util.logging.Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                        java.util.logging.Logger.getLogger(Mahasiswa.class.getName())
                                        .log(java.util.logging.Level.SEVERE, null, ex);
                } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                        java.util.logging.Logger.getLogger(Mahasiswa.class.getName())
                                        .log(java.util.logging.Level.SEVERE, null, ex);
                }
                // </editor-fold>

                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                new Mahasiswa().setVisible(true);
                        }
                });
        }

        // Variables declaration - do not modify//GEN-BEGIN:variables
        private javax.swing.JPanel AddMahasiswa;
        private javax.swing.JLabel MahasiswaMenu;
        private javax.swing.JPanel Main;
        private javax.swing.JRadioButton RdTaat;
        private javax.swing.JRadioButton Rdaktif;
        private javax.swing.JRadioButton RdpTdkA;
        private javax.swing.JRadioButton RdtdkT;
        private javax.swing.JLabel alamatLabel1;
        private javax.swing.JButton btnBack;
        private javax.swing.JButton btnBatal;
        private javax.swing.JButton btnEdit;
        private javax.swing.JButton btnHapus;
        private javax.swing.JButton btnRefresh;
        private javax.swing.JButton btnSimpan;
        private javax.swing.JTextField isianMK1;
        private javax.swing.JTextField isianMK2;
        private javax.swing.JTextField isianMK3;
        private javax.swing.JTextField isianMK4;
        private javax.swing.JComboBox<String> isianMatakuliah;
        private javax.swing.JComboBox<String> isianProdi;
        private javax.swing.JLabel jLabel10;
        private javax.swing.JLabel jLabel2;
        private javax.swing.JLabel jLabel3;
        private javax.swing.JLabel jLabel4;
        private javax.swing.JLabel jLabel5;
        private javax.swing.JLabel jLabel6;
        private javax.swing.JLabel jLabel7;
        private javax.swing.JLabel jLabel8;
        private javax.swing.JScrollPane jScrollPane1;
        private javax.swing.JTextField kelasAdd;
        private javax.swing.JTextField labelAddnama;
        private javax.swing.JLabel labelMenit;
        private javax.swing.JLabel labelNama;
        private javax.swing.JTextField labelNim;
        private javax.swing.JLabel labelTanggal;
        private javax.swing.JLabel labelWelcome;
        private javax.swing.JLabel labelid;
        private javax.swing.JButton logout;
        private javax.swing.JLabel mataKLabel;
        private javax.swing.JLabel namaLabel;
        private javax.swing.JLabel nimLabel;
        private javax.swing.JLabel p1Label;
        private javax.swing.JLabel p2Label;
        private javax.swing.JLabel p3Label;
        private javax.swing.JLabel p4Label;
        private javax.swing.JLabel prodiLabel;
        private javax.swing.JTable tableMahasiswa;
        private javax.swing.JLabel tambahmaha;
        private javax.swing.JLabel tambahmaha1;
        private javax.swing.JPanel topbar;
        // End of variables declaration//GEN-END:variables
}
